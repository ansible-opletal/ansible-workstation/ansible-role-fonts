ansible-role-fonts
=========

This role installs font packages based on os_family. Which fonts get downloaded
can be configured under vars/{{ ansible_facts['os_family'] }} for each family

Requirements
------------

None

Role Variables
--------------

Self-explanatory

Dependencies
------------

None, is a dependency to [ansible-linux-workstation](https://gitlab.com/ansible-opletal/ansible-workstation/ansible-linux-workstation)

Example Playbook
----------------

Visit [ansible-linux-workstation](https://gitlab.com/ansible-opletal/ansible-workstation/ansible-linux-workstation)

License
-------

BSD
